<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Public routes
Route::post('/login', 'Auth\LoginController@authenticate');
Route::post('/register', 'Auth\RegisterController@store');

Route::middleware('api')->get('/galleries', 'GalleriesController@index');
Route::middleware('api')->get('/galleries/{id}', 'GalleriesController@show');
Route::middleware('api')->get('/usersgalleries/{user_id}', 'GalleriesController@usersgalleries');

//jwt protected routes
Route::middleware('jwt')->post('/galleries', 'GalleriesController@store');
Route::middleware('jwt')->delete('/galleries/{id}', 'GalleriesController@destroy');
Route::middleware('jwt')->get('/usersgalleries', 'GalleriesController@ownergalleries');
Route::middleware('jwt')->put('/galleries/{id}', 'GalleriesController@update');

Route::middleware('jwt')->post('/comment/{id}', 'CommentsController@store');
Route::middleware('jwt')->delete('/comment/{id}', 'CommentsController@destroy');