<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Validator;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\JsonResponse;

class CommentsController extends Controller
{
    public function store(Request $request, $id, JWTAuth $auth)
    {
        $comment= new Comment();

        $validator = Validator::make($request->all(), [
            'content' => 'required|max:1000',
        ]);
        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), 400);
        }

        $comment->gallery_id = $id;
        $comment->user_id = $auth->parseToken()->toUser()->id;
        $comment->content = $request->input('content');

        $comment->save();
        return $comment;
    }
    public function destroy($id)
    {
        $comment = Comment::find($id);
        
        $comment->delete();

        return new JsonResponse(true);
    }
}
