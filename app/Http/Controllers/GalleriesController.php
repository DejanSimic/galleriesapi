<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use Validator;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\JsonResponse;

class GalleriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Gallery::with('images')->with('user')->get();
    }
    public function ownergalleries(JWTAuth $auth)
    {
        $authUserId = $auth->parseToken()->toUser()->id;
        return Gallery::with('images')->with('user')->where('user_id', $authUserId)->get();
    }
    public function usersgalleries($user_id)
    {
        return Gallery::with('images')->with('user')->where('user_id', $user_id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, JWTAuth $auth)
    {
        $gallery= new Gallery();

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:galleries|min:2|max:255',
            'description' => 'required|max:1000',
            'imageUrl' => 'required'
        ]);
        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), 400);
        }

        $gallery->user_id = $auth->parseToken()->toUser()->id;
        $gallery->name = $request->input('name');
        $gallery->description = $request->input('description');
        $imageUrls = $request->input('imageUrl');
        
        $gallery->save();
        foreach($imageUrls as $imageUrl)
        $gallery->images()->create(['imageUrl' => $imageUrl]);
        return $gallery;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, JWTAuth $auth)
    {
        try {
        $user_id = $auth->parseToken()->toUser()->id;
        if ($user_id) {

        }
        else{
            $user_id = 0;
        }
        }catch(\Tymon\JWTAuth\Exceptions\JWTException $e){//general JWT exception
            $user_id = 0;
        }
        $gallery = Gallery::with('images')->with('user')->with('comments', 'comments.user')->find($id);
        return response()->json(compact(['gallery', 'user_id']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, JWTAuth $auth)
    {
        $gallery= Gallery::find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'imageUrl' => 'required'
        ]);
        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), 400);
        }

        $gallery->user_id = $auth->parseToken()->toUser()->id;
        $gallery->name = $request->input('name');
        $gallery->description = $request->input('description');
        $imageUrls = $request->input('imageUrl');

        //Delete current images from database
        foreach($gallery->images as $image)
        $image->delete();
        
        $gallery->save();

        //Add images again to database with diferent id-s
        foreach($imageUrls as $imageUrl)
        $gallery->images()->create(['imageUrl' => $imageUrl]);
        return $gallery;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, JWTAuth $auth)
    {
        $gallery = Gallery::with('user')->find($id);
        $userId = $auth->parseToken()->toUser()->id;
        
        if($gallery->user->id === $userId)
        {
            $gallery->delete();
            return new JsonResponse(true);
        }
        else return new JsonResponse($id, 403);
    }
}
